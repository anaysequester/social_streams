<?php
require( dirname(__FILE__) . '/wp-load.php' );
require( dirname( __FILE__ ) . '/wp-blog-header.php' );

// set the feed url you want
$feed_contents=file_get_contents("json_feed.php"); 
$arr = json_decode ($feed_contents,1);
// set the metadata fields you want
$post_array=array("title","author","content");

// set the body fields you want
$content_array=array("text1","text2");
$break="\n";
for($i=0;$i<count($arr);$i++){
 $description="";
 $description=$arr[$i]['content'];
 foreach($arr[$i] as $key => $val){
  if (in_array($key, $content_array)) {
     $description .= $break;
     $description .= $val;
   }
  }
  
 if( null == get_page_by_title( $arr[$i]['title'] ) ) {
	 $post_id = wp_insert_post(
		array(
			'comment_status'	=>	'closed',
			'ping_status'		=>	'closed',
			'post_author'		=>	$arr[$i]['author'],
			'post_name'		=>	sanitize_title($arr[$i]['title']),
			'post_title'		=>	$arr[$i]['title'],
      'post_content'  =>  $description,
			'post_status'		=>	'publish',
			'post_type'		=>	'post'
		)
	);
  foreach($arr[$i] as $key => $val){

  if (!in_array($key, $post_array)) {
     add_post_meta($post_id, $key, $val);
   }
  }
  echo " -- ".$arr[$i]['title']."<br />"; 
  }
}
?>